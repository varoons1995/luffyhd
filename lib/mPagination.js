function getPagination(currentPage=1, maxPage=0, basedUrl='/', option={}){
    let li = '';
    let ul, nav, prev_btn, last_btn;
    let a_class = Array.isArray(option.a_class)?option.a_class.join(' '):'';
    let li_class = Array.isArray(option.li_class)?option.li_class.join(' '):'';
    let ul_class = Array.isArray(option.ul_class)?option.ul_class.join(' '):'';
    let nav_class = Array.isArray(option.nav_class)?option.nav_class.join(' '):'';
    let page_start = currentPage-2>0?currentPage-2:1;
    let page_end = currentPage+2<maxPage?currentPage+2:maxPage;

    for(let i=page_start;i<=page_end;i++){
        if(i===currentPage){
            li += `<li class="${li_class} active"><span class="${a_class}">${i}<span class="sr-only">(current)</span></span></li>`;
            continue;
        }
        li += `<li class="${li_class}"><a class="${a_class}" href="${basedUrl}?page=${i}">${i}</a></li>`;
    }
    

    prev_btn = (currentPage===1)?'':`<li class="${li_class}">
                    <a class="${a_class}" href="${basedUrl}?page=1" aria-label="Next">&laquo;</a>
                </li>
                <li class="${li_class}">
                    <a class="${a_class}" href="${basedUrl}?page=${currentPage-1}" aria-label="Next">&lsaquo;</a>
                </li>`;
    last_btn = (currentPage===maxPage || maxPage===0)?'':`<li class="${li_class}">
                    <a class="${a_class}" href="${basedUrl}?page=${currentPage+1}" aria-label="Next">&rsaquo;</a>
                </li>
                <li class="${li_class}">
                    <a class="${a_class}" href="${basedUrl}?page=${maxPage}" aria-label="Next">&raquo;</a>
                </li>`;
    ul = `<ul class="${ul_class}">${prev_btn}${li}${last_btn}</ul>`;
    nav = `<nav aria-label="..." class="${nav_class}">${ul}</nav>`;

    return nav;
}

module.exports = {
    getPagination
}