const db = require('./db');
const knex = require('knex')(db.option);

async function getAll(offset=0) {
    return knex('movie').select().orderBy('created_date', 'desc')
    .limit(21).offset(offset);
}

async function getAllBySearch(offset=0, search='a') {
    return knex('movie').where('title', 'like', `%${search}%`).select().orderBy('created_date', 'desc')
    .limit(21).offset(offset);
}

async function countAllBySearch(search='a') {
    return knex('movie').where('title', 'like', `%${search}%`).count('*', {as: 'count'});
}

async function getCountAll() {
    return await knex('movie').count('*', {as: 'count'});
}

async function getByTitle(title='') {
    return knex('movie')
    .where({'title': title}).select();
}

async function getByTitle01(title='') {
    return knex('movie')
    .where({'title': title}).select()
    .then((value) => {
        console.log('getByTitle01');
        console.log(value);
    });
}

async function getNewMovie() {
    return knex('movie').select().orderBy('created_date', 'desc').limit(5);
}

async function getCategory() {
    return knex('category').select();
}

async function getByCategory(offset=0, category='') {
    return knex('category').where({'.category.category': category}).select().orderBy('created_date', 'desc')
        .innerJoin('movie_category', 'category.id', 'movie_category.category_id')
        .innerJoin('movie', 'movie_category.movie_id', 'movie.id')
        .limit(21).offset(offset);
}

async function countByCategory(category='') {
    return knex('category').where({'.category.category': category}).count('*', {as: 'count'})
        .innerJoin('movie_category', 'category.id', 'movie_category.category_id')
        .innerJoin('movie', 'movie_category.movie_id', 'movie.id');
}

async function getVideoByMovieId(movieId=0) {
    return knex('movie_video')
    .where({'movie_id': movieId}).select()
}

// async function insertCategoryByMovieId(cate='',id=''){
//     return knex('movie_category').insert({movie_id:id,category_id:cate}).catch(function (result){
//       console.log('Add movie '+id+ 'Category '+ cate)    
//    }); 
// }

// async function insertVideo(site=''){
//    return knex('video').insert({which_url:site}).catch(function (result){
//        console.log('Add Url'+Url);    
//     }); 
// }

// async function insertMovieVideoByMovieId({movie_id='',video_id='',url=''}){
//    return knex('movie_video').insert({movie_id:movie_id,video_id:video_id,url:url}).catch(function (result){
//        console.log('Add Url'+Url);    
//     }); 
// }

async function insertMovie(title='',cover='',category='',which_url='',url=''){
       knex('movie').insert({title:title,cover:cover}).catch(function (result){
        var i = ''// movie id after insert to table

        if(category != null && category != undefined ){
            for(let cate in category){
                knex('movie_category').insert({movie_id:id,category_id:cate}).catch(function (result){
                    console.log('Add Category '+cate+ 'in Moive '+ movie_id)    
                 }); 
            }
        }

        if(which_url != null || which_url != undefined){
            for(let site in which_url){
                knex('video').insert({which_url:site}).catch(function (result){
                    console.log('Add Video')
                    var videoId = ''// video id after insert to table  
                    knex('movie_video').insert({})    
                     }); 
                }
        }
    
    });
}

async function insertCategory(category=''){
    knex('category').insert({category:category}).catch(function (result){
    });
}

async function getMovie12item(){
   return knex('movie').orderByRaw('RAND()').limit(4);
}

async function getcategoryByMovieId(movieId=0) {
    return knex('movie_category')
    .innerJoin('category', 'category.id', 'movie_category.category_id')
    .where({'movie_id': movieId}).select()
}

module.exports = {
    getAll,
    getCountAll,
    getByTitle,
    getCategory,
    getByCategory,
    getNewMovie,
    getAllBySearch,
    countAllBySearch,
    countByCategory,
    insertMovie,
    insertCategory,
    getMovie12item,
    getByTitle01,
    getVideoByMovieId,
    getcategoryByMovieId
}
