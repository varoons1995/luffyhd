const homeRoutes = {};
const movie = require('../../lib/dbmovie');
const util = require('../../lib/utility');
const mPagination = require('../../lib/mPagination');


homeRoutes.get = (req, res) => {
	let currentPage = util.toNum( req.query.page, 1 );
	let offset = (currentPage-1)*21;
	Promise.all([movie.getAll(offset), movie.getCountAll(), movie.getCategory() , movie.getMovie12item() , movie.getMovie12item(),movie.getMovie12item()])
	.then((values) => {
		let list_movie = values[0];
		let count = values[1][0].count;
		let maxPage = Math.ceil(count/21);
		let category = values[2];
		let movie1 = values[3];
		let movie2 = values[4];
		let movie3 = values[5];
		let basedUrl = req._parsedOriginalUrl.pathname;
		let paginateOption = {
			a_class: ['page-link'],
			li_class: ['page-item'],
			ul_class: ['pagination', 'justify-content-center']
		};
		let nav_pagination = mPagination.getPagination(currentPage, maxPage, basedUrl, paginateOption);

		res.render('listMovie', {
			req: req,
			list_movie: list_movie,
			category: category,
			movie1: movie1,
			movie2 : movie2,
			movie3 : movie3,
			nav_pagination: nav_pagination
		});

	})
	.catch(error => { 
		console.error(error.message);
		res.render('error');
	});
	
}

module.exports = homeRoutes;
